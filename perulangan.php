<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perulangan</title>
    <link rel="stylesheet" href="file.css">
</head>
<body>
    <header class="header">
        <a href="home.html" class="logo">M.Raeki</a>
        <nav class="navbar">
            <a href="Website.html">Home</a>
            <a href="perulangan.php">Perulangan</a>
            <a href="percabangan.php">Percabangan</a>
        </nav>
    </header>

    <main>
        <article id="conten">
            <div class="conten-left">
                <h2>Form Input Tinggi Segitiga</h2>
                <form method="post" onsubmit="return validateForm()">
                    <label for="tinggi">Tinggi:</label>
                    <input type="number" id="tinggi" name="tinggi" required min="1"><br><br>
                    <input type="submit" value="Submit">
                </form>

                <?php
                function cetakSegitiga($tinggi){
                    for ($i = 1; $i <= $tinggi; $i++) {
                        for ($k = $tinggi; $k > $i; $k--) {
                            echo "&nbsp&nbsp";
                        }
                        for ($j = 1; $j <= (2 * $i - 1); $j++) {
                            echo "*";
                        }
                        echo "<br>";
                    }
                }

                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $tinggi = htmlspecialchars($_POST['tinggi']);
                    cetakSegitiga($tinggi);
                }
                ?>
            </div>
        </article>
    </main>

    <footer>
        <div class="bawah">
            <p>@2024 M.Raeki</p>
        </div>
    </footer>
</body>
</html>